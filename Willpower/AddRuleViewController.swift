//
//  AddRuleViewController.swift
//  Willpower
//
//  Created by Chris Dolce on 2/29/16.
//  Copyright © 2016 Chris Dolce. All rights reserved.
//

import UIKit
import RealmSwift

class AddRuleViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextViewDelegate{
    
    @IBOutlet weak var ruleTextView: UITextView!
    @IBOutlet weak var xpTextField: UITextField!
    @IBOutlet weak var isGoodRule: UISwitch!
    
    let XPArray = [1,2,3,5,7,10,15,20]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ruleTextView!.delegate = self
        
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var addRuleTextView: UITextView!
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func DismissController(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func saveRule(_ sender: AnyObject)
    {
        
        if ruleTextView.text == "" || ruleTextView.text == "Add Rule" || xpTextField.text == "" || xpTextField.text == "This"
        {
            let alert = UIAlertController(title: "Wait", message: "One or more firlds are empty!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            if ruleTextView.text == "" || ruleTextView.text == "Add Rule"{
                ruleTextView.textColor = UIColor.red
                ruleTextView.font = UIFont.boldSystemFont(ofSize: 16.0)
                ruleTextView.text = "This field"
            }
            if xpTextField.text == ""
            {
                xpTextField.textColor = UIColor.red
                xpTextField.font = UIFont.boldSystemFont(ofSize: 16.0)
                xpTextField.text = "This"
            }
        }
        else{
            let realm = try! Realm()
            let rule = Rules()
            var blah = Int(xpTextField.text!)!
            if isGoodRule.isOn == false
            {
                blah = blah * -1
            }
            rule.XPReward = blah
            rule.rule = ruleTextView.text
            rule.happensOnce = true
            rule.amountHappened = 1
            rule.isGood = isGoodRule.isOn
            rule.ruleIncrement = 0
            
            try! realm.write()
            {
                realm.add(rule)
            }
            sortRules()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func sortRules()
    {
        let allRealm = try! Realm()
        let allRules = allRealm.objects(Rules.self)
        var rulesArray: [Rules]
        rulesArray = [Rules]()
        for i in 0 ..< allRules.count
        {
            let tempRule = Rules()
            tempRule.amountHappened = allRules[i].amountHappened
            tempRule.happensOnce = allRules[i].happensOnce
            tempRule.isGood = allRules[i].isGood
            tempRule.rule = allRules[i].rule
            tempRule.ruleIncrement = allRules[i].ruleIncrement
            tempRule.XPReward = allRules[i].XPReward
            rulesArray.append(tempRule)
        }
        try! allRealm.write()
            {
                allRealm.delete(allRules)
        }
        rulesArray.sort(by: {$0.0.XPReward < $0.1.XPReward})
        for i in 0 ..< rulesArray.count
        {
            let realm = try! Realm()
            let rules = Rules()
            rules.amountHappened = rulesArray[i].amountHappened
            rules.happensOnce = rulesArray[i].happensOnce
            rules.isGood = rulesArray[i].isGood
            rules.rule = rulesArray[i].rule
            rules.ruleIncrement = rulesArray[i].ruleIncrement
            rules.XPReward = rulesArray[i].XPReward
            try! realm.write() {
                realm.add(rules)
            }
        }

    }
    
    func keyboardDoneButtonTapped()
    {
        ruleTextView.resignFirstResponder()
        xpTextField.resignFirstResponder()
    }
    
    // Override Functions
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let picker : UIPickerView = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        textField.inputView = picker
        xpTextField.textColor = UIColor.black
        xpTextField.font = UIFont.systemFont(ofSize: 16.0)
        if xpTextField.text == "This"{
            xpTextField.text = ""
        }
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddRuleViewController.keyboardDoneButtonTapped))]
        numberToolbar.sizeToFit()
        xpTextField.inputAccessoryView = numberToolbar
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        ruleTextView.textColor = UIColor.black
        ruleTextView.font = UIFont.systemFont(ofSize: 16.0)
        if ruleTextView.text == "This field" || ruleTextView.text == "Add Rule"
        {
            ruleTextView.text = ""
        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddRuleViewController.keyboardDoneButtonTapped))]
        numberToolbar.sizeToFit()
        ruleTextView.inputAccessoryView = numberToolbar
        return true
    }
    
    func numberOfComponents(in colorPicker: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return XPArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return String(XPArray[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        xpTextField.text = String(XPArray[row])
    }
}
