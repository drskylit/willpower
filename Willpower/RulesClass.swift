//
//  RulesClass.swift
//  Willpower
//
//  Created by Chris Dolce on 2/24/16.
//  Copyright © 2016 Chris Dolce. All rights reserved.
//

import Foundation
import RealmSwift
class Rules: Object{
    dynamic var XPReward :Int = -1
    dynamic var rule :String = ""
    dynamic var happensOnce :Bool = false
    dynamic var amountHappened :Int = 0
    dynamic var isGood :Bool = false
    dynamic var ruleIncrement :Int = 0
}