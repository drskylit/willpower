//
//  CustomTableViewCell.swift
//  Willpower
//
//  Created by Chris Dolce on 3/31/16.
//  Copyright © 2016 Chris Dolce. All rights reserved.
//

import UIKit
import RealmSwift
class CustomTableViewCell: UITableViewCell
{

    var rules: Results<Rules>!
    var player: Results<Player>!
    var selectedRow = 0
    var realm: Realm!
    
    @IBOutlet weak var RuleTextView: UITextView!
    @IBOutlet weak var XPLabel: UILabel!
    @IBOutlet weak var AMTCounter: UIStepper!
    @IBOutlet weak var AMTNumberLabel: UILabel!
    var row: Int = 0
    var oldValue :Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        realm = try! Realm()
        rules = realm.objects(Rules.self)
        player = realm.objects(Player.self)
        oldValue = Int(AMTCounter.value);
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func Counter(_ sender: AnyObject)
    {
        AMTNumberLabel.text = String(Int(AMTCounter.value))
        
        if (Int(AMTCounter.value)>oldValue)
        {
            oldValue=oldValue+1;
            try! realm.write()
            {
                rules[row].ruleIncrement = Int(AMTCounter.value)
                player[0].currXP = player[0].currXP + rules[row].XPReward
                if rules[row].isGood == true
                {
                    player[0].goodXP = player[0].goodXP + rules[row].XPReward
                }
                else
                {
                    player[0].badXP = player[0].badXP - rules[row].XPReward
                }
            }
            //Your Code You Wanted To Perform On Increment
        }
            
        else
        {
            oldValue=oldValue-1;
            try! realm.write()
                {
                    rules[row].ruleIncrement = Int(AMTCounter.value)
                    player[0].currXP = player[0].currXP - rules[row].XPReward
                if rules[row].isGood == true
                {
                    player[0].goodXP = player[0].goodXP - rules[row].XPReward
                }
                else
                {
                    player[0].badXP = player[0].badXP + rules[row].XPReward
                }
            }
            //Your Code You Wanted To Perform On Decrement
        }
        
    }
}
