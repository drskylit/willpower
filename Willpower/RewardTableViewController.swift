//
//  RewardTableViewController.swift
//  Willpower
//
//  Created by Chris Dolce on 3/21/16.
//  Copyright © 2016 Chris Dolce. All rights reserved.
//

import UIKit
import RealmSwift
class RewardTableViewController: UITableViewController
{

    var rewards: Results<Rewards>!
    var player: Results<Player>!
    var selectedRow = 0
    var realm: Realm!
    
    @IBOutlet var RewardTable: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        realm = try! Realm()
        rewards = realm.objects(Rewards.self)
        player = realm.objects(Player.self)
        RewardTable.reloadData()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func longPressGestureRecognized(_ gestureRecognizer: UIGestureRecognizer) {
        
        print(selectedRow)

        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        return rewards.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        // Configure the cell...
        cell.textLabel?.text = rewards[indexPath.row].reward
        cell.detailTextLabel?.text = String(rewards[indexPath.row].XPNeeded)
        if rewards[indexPath.row].hasClaimed == true
        {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(RewardTableViewController.longPressGestureRecognized(_:)))
        cell.addGestureRecognizer(longpress)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        selectedRow = indexPath.row
        print("Curr XP",player[0].currXP);
        print("Next XP",player[0].nextLevelXP);
        if player[0].currXP >= player[0].nextLevelXP
        {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            try! realm.write() {
                rewards[indexPath.row].hasClaimed = true
            }
        }
        else if player[0].currXP < player[0].nextLevelXP && rewards[indexPath.row].hasClaimed != true
        {
            cell.textLabel?.text = rewards[indexPath.row].reward
            cell.detailTextLabel?.text = String(rewards[indexPath.row].XPNeeded)
            cell.accessoryType = UITableViewCellAccessoryType.none
            let alert = UIAlertController(title: "you do not have enough XP", message: "Do more good things to claim your reward!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "I Will!", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }
        
        if rewards[indexPath.row].hasClaimed == true {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        
        cell.textLabel?.text = rewards[indexPath.row].reward
        cell.detailTextLabel?.text = String(rewards[indexPath.row].XPNeeded)
        RewardTable.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("wdjkwaefbkjawf")
        RewardTable.deselectRow(at: indexPath, animated: true);
    }
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            let allRealm = try! Realm()
            let allRewards = allRealm.objects(Rewards.self)
            try! allRealm.write()
                {
                    allRealm.delete(allRewards[indexPath.row])
            }
        }
        // Delete the row from the data source
        tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
}
