//
//  ViewController.swift
//  Willpower
//
//  Created by Chris Dolce on 2/24/16.
//  Copyright © 2016 Chris Dolce. All rights reserved.
//

import UIKit
import RealmSwift
class ViewController: UIViewController , UIAlertViewDelegate
{

    var player: Results<Player>!
    var rewards: Results<Rewards>!
    var selectedRow = 0
    var realm: Realm!
    
    @IBOutlet weak var currLevelLabel: UILabel!
    @IBOutlet weak var nextLevelInLabel: UILabel!
    @IBOutlet weak var currXPLabel: UILabel!
    @IBOutlet weak var levelSlider: UISlider!
    @IBOutlet weak var goodXPLabel: UILabel!
    @IBOutlet weak var badXPLabel: UILabel!
    @IBOutlet weak var nextRewardTextView: UITextView!
    @IBOutlet weak var nextRewardLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        realm = try! Realm()
        player = realm.objects(Player.self)
        rewards = realm.objects(Rewards.self)
        if(!player.isEmpty)
        {
            if player[0].nextLevelXP == player[0].currXP && player[0].nextReward != "No Rewards to show"
            {
                let alert = UIAlertController(title: "YOU HAVE DONE IT", message: "You have gained " + player[0].nextReward, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        
        realm = try! Realm()
        player = realm.objects(Player.self)
        rewards = realm.objects(Rewards.self)
        
//        if player[0].nextLevelXP == player[0].currXP
//        {
//            let alert = UIAlertController(title: "YOU HAVE DONE IT", message: "Yopu have gained a reward", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler: nil))
//            self.presentViewController(alert, animated: true, completion: nil)
//
//        }
        
        if(player.isEmpty)
        {
            let playa = Player()
            playa.name = "Chris"
            try! realm.write() {
                realm.add(playa)
            }
            player = realm.objects(Player.self)
        }
        else
        {
            if rewards.count != 0
            {
                for i in 0 ..< rewards.count
                {
                    if rewards[i].hasClaimed != true
                    {
                        try! realm.write()
                        {
                            player[0].nextReward = rewards[i].reward
                            player[0].nextLevelXP = rewards[i].XPNeeded
                        }
                        break;
                    }
                    else
                    {
                        try! realm.write()
                        {
                            player[0].nextReward = "No Rewards to show"
                            player[0].nextLevelXP = 0
                            player[0].currLevel = i+1;
                        }
                        
                    }
                }
            }
            else
            {
                try! realm.write()
                {
                    player[0].nextReward = "No Rewards to show"
                    player[0].nextLevelXP = 0
                }
            }
        }

        
        currLevelLabel.text = "Current Level:\(player[0].currLevel)"
        nextLevelInLabel.text = "Next Level in:\(player[0].nextLevelXP)"
        currXPLabel.text = "Current XP:\(player[0].currXP)"
        levelSlider.minimumValue = Float(player[0].lastLevelXP)
        levelSlider.maximumValue = Float(player[0].nextLevelXP)
        levelSlider.setValue(Float(player[0].currXP), animated:true)
        goodXPLabel.text = "Good Xp:\(player[0].goodXP)"
        badXPLabel.text = "Bad Xp:\(player[0].badXP)"
        nextRewardTextView.text = "\(player[0].nextReward)"
        nextRewardLabel.text = "Next Reward:\(player[0].nextLevelXP)"

    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     
    
}

