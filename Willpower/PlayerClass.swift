//
//  PlayerClass.swift
//  Willpower
//
//  Created by Chris Dolce on 2/24/16.
//  Copyright © 2016 Chris Dolce. All rights reserved.
//

import Foundation
import RealmSwift

class Player: Object{
    dynamic var name :String!;
    dynamic var currLevel :Int = 0
    dynamic var nextLevelXP :Int = 0;
    dynamic var currXP :Int = 0
    dynamic var lastLevelXP :Int = 0;
    dynamic var goodXP :Int = 0
    dynamic var badXP :Int = 0
    dynamic var nextReward :String = " "
}