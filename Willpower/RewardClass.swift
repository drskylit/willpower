//
//  RewardClass.swift
//  Willpower
//
//  Created by Chris Dolce on 2/24/16.
//  Copyright © 2016 Chris Dolce. All rights reserved.
//

import Foundation
import RealmSwift

class Rewards:Object{
    dynamic var XPNeeded :Int = 0
    dynamic var reward :String = ""
    dynamic var hasClaimed :Bool = false;
}