//
//  RewardViewController.swift
//  Willpower
//
//  Created by Chris Dolce on 3/21/16.
//  Copyright © 2016 Chris Dolce. All rights reserved.
//

import UIKit
import RealmSwift
class RewardViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var xpNeededTextField: UITextField!
    @IBOutlet weak var addRewardTextView: UITextView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addRewardTextView.delegate = self
        xpNeededTextField.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func DismissController(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func addReward(_ sender: AnyObject) {
       
        if addRewardTextView.text == "" || addRewardTextView.text == "Add Reward" || xpNeededTextField.text == "" || xpNeededTextField.text == "This"
        {
            let alert = UIAlertController(title: "Wait", message: "One or more firlds are empty!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            if addRewardTextView.text == "" || addRewardTextView.text == "Add Reward"
            {
                addRewardTextView.textColor = UIColor.red
                addRewardTextView.font = UIFont.boldSystemFont(ofSize: 16.0)
                addRewardTextView.text = "This field"
            }
            if xpNeededTextField.text == ""
            {
                xpNeededTextField.textColor = UIColor.red
                xpNeededTextField.font = UIFont.boldSystemFont(ofSize: 16.0)
                xpNeededTextField.text = "This"
            }
        }
        else
        {
            let realm = try! Realm()
            let rewards = Rewards()
            rewards.XPNeeded = Int(xpNeededTextField.text!)!
            rewards.reward = addRewardTextView.text
            rewards.hasClaimed = false
            try! realm.write()
            {
                realm.add(rewards)
            }
            sortRewards()
             self.navigationController?.popViewController(animated: true)
        }
    }
    
    func keyboardDoneButtonTapped()
    {
        addRewardTextView.resignFirstResponder()
        xpNeededTextField.resignFirstResponder()
    }
    
    func sortRewards()
    {
        let allRealm = try! Realm()
        let allRewards = allRealm.objects(Rewards.self)
        var rewardsArray: [Rewards]
        rewardsArray = [Rewards]()
        for i in 0 ..< allRewards.count
        {
            let tempReward = Rewards()
            tempReward.XPNeeded = allRewards[i].XPNeeded
            tempReward.reward = allRewards[i].reward
            tempReward.hasClaimed = allRewards[i].hasClaimed
            rewardsArray.append(tempReward)
        }
        try! allRealm.write()
        {
            allRealm.delete(allRewards)
        }
        rewardsArray.sort(by: {$0.0.XPNeeded < $0.1.XPNeeded})
        for i in 0 ..< rewardsArray.count
        {
            let realm = try! Realm()
            let rewards = Rewards()
            rewards.XPNeeded = rewardsArray[i].XPNeeded
            rewards.reward = rewardsArray[i].reward
            rewards.hasClaimed = rewardsArray[i].hasClaimed
            try! realm.write() {
                realm.add(rewards)
            }
        }
        var player: Results<Player>!
        var rewards: Results<Rewards>!
        let realm = try! Realm()
        player = realm.objects(Player.self)
        rewards = realm.objects(Rewards.self)
        for i in 0 ..< allRewards.count
        {
            if rewards[i].hasClaimed != true
            {
                try! realm.write()
                {
                        player[0].nextLevelXP = rewards[i].XPNeeded;
                }
            }
        }
        
    }
    // Override Functions
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        xpNeededTextField.textColor = UIColor.black
        xpNeededTextField.font = UIFont.systemFont(ofSize: 16.0)
        if xpNeededTextField.text == "This"
        {
            xpNeededTextField.text = ""
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddRuleViewController.keyboardDoneButtonTapped))]
        numberToolbar.sizeToFit()
        xpNeededTextField.inputAccessoryView = numberToolbar
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        addRewardTextView.textColor = UIColor.black
        addRewardTextView.font = UIFont.systemFont(ofSize: 16.0)
        if addRewardTextView.text == "This field" || addRewardTextView.text == "Add Reward"
        {
            addRewardTextView.text = ""
        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddRuleViewController.keyboardDoneButtonTapped))]
        numberToolbar.sizeToFit()
        addRewardTextView.inputAccessoryView = numberToolbar
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
